// const fs = require('fs');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDev = process.env.NODE_ENV !== 'production';
// const ROOT_DIR = fs.realpathSync(process.cwd());

function pathResolve(...args) {
  return path.resolve(...args);
}

function getName(ext) {
  return `[name].[${isDev ? 'hash:8' : 'contenthash'}].bundle.${ext}`;
}

module.exports = {
  mode: isDev ? 'development' : 'production',
  entry: {
    main: './src/index.jsx',
  },
  output: {
    filename: getName('js'),
    path: pathResolve('dist'),
  },
  resolve: {
    extensions: ['.js', '.json', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/i,
        exclude: [/node_modules/],
        use: ['babel-loader'],
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.s[ac]ss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/i,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader'],
      },
      /* {
                test: /\.svg$/,
                loader: 'svg-inline-loader'
            } */
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: pathResolve('public/index.html'),
    }),
    new MiniCssExtractPlugin(),
  ],
  devServer: {
    port: 3000,
    open: true,
  },
};
