import React from 'react';

export function ProductName() {
  return (
    <div className="product-name">
      <div className="container ">
        <div className="row justify-content-between text-white  py-5">

          <div className="col-md-12">
            <div className="row">
              <div className="col-md-7">
                <h1 className="mb-3 font-size-60">Product name</h1>
                <ul className="font-size-20 pl-0">
                  <li><i className="fas fa-check" /> Put on this page information about your product</li>
                  <li><i className="fas fa-check" /> A detailed description of your product</li>
                  <li><i className="fas fa-check" /> Tell us about the advantages and merits</li>
                  <li><i className="fas fa-check" /> Associate the page with the payment system</li>
                </ul>
              </div>
              <div className="col-md-5">
                <div className="product-name-img mr-0" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
