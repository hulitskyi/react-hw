import React from 'react';

export function Screenshots() {
  return (
    <div className="screenshots py-5">
      <div className="container">
        <h2 className="text-center font-size-30 mb-4">Screenshots</h2>
        <div className="row">
          <div className="col-xl-6">
            <ListItem />
            <ListItem />
          </div>
          <div className="col-xl-6">
            <ListItem />
            <ListItem />
          </div>
        </div>
      </div>
    </div>
  );
}

function ListItem() {
  return (
    <div className="screenshot-item">
      <div className="screenshot">
        <img alt="" />
      </div>
      <div className="screenshot-item_text">
        <div className="screenshot-item_title">
          The description of the image
        </div>
        <div className="screenshot-item_desc">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga, illo at. Natus eos,
            eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut ipsum.
          </p>
        </div>
      </div>
    </div>
  );
}
