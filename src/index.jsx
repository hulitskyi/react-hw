import React from 'react';
import ReactDOM from 'react-dom';

import '@fortawesome/fontawesome-free/js/fontawesome';
import '@fortawesome/fontawesome-free/js/solid';
import '@fortawesome/fontawesome-free/js/regular';
import '@fortawesome/fontawesome-free/js/brands';

import { ProductName } from './product-name';
import { AboutProduct } from './about-product';
import { DignityAndPluses } from './dignity-and-pluses';
import { Screenshots } from './screenshots';
import { Reviews } from './reviews';
import { BuyItNow } from './buy-it-now';
import { Contacts } from './contacts';

import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.scss';
import './css/App.scss';

ReactDOM.render(
  <React.StrictMode>
    <ProductName />
    <AboutProduct />
    <DignityAndPluses />
    <Screenshots />
    <Reviews />
    <BuyItNow />
    <Contacts />
  </React.StrictMode>,
  document.getElementById('root'),
);
