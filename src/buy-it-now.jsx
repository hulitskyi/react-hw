import React from 'react';

export function BuyItNow() {
  return (
    <div className="buy-it-now-row py-5">
      <div className="container">
        <h2 className="text-center font-size-30 mb-4">Buy it now</h2>
        <div className="row align-items-end justify-content-around buy-it-now__offers">
          <div className="col-xl-3 buy-it-now__standard-offer pb-3 mb-xl-0 mb-4">
            <h3 className="text-white text-center py-2"> Standart</h3>
            <div className="offers-price">
              <h3 className="text-center pt-3 font-size-30">$100</h3>
            </div>
            <div className="offers__list-of-benefits text-white">
              <ol className="pl-4">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
              </ol>
              <input type="button" className="buy-it-now__button bg-white" value="Buy" />
            </div>
          </div>
          <div className="col-xl-3 buy-it-now__premium-offer pb-3 mb-xl-0 mb-4">
            <h3 className="text-white text-center py-2"> Premium</h3>
            <div className="offers-price">
              <h3 className="text-center pt-3 font-size-30">$150</h3>
            </div>
            <div className="offers__list-of-benefits text-white">
              <ol className="pl-4">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
              </ol>
              <input type="button" className="buy-it-now__button bg-white" value="Buy" />
            </div>
          </div>
          <div className="col-xl-3 buy-it-now__lux-offer pb-3 mb-xl-0 mb-4">
            <h3 className="text-white text-center py-2"> Premium</h3>
            <div className="offers-price">
              <h3 className="text-center pt-3 font-size-30">$200</h3>
            </div>
            <div className="offers__list-of-benefits text-white">
              <ol className="pl-4">
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
                <li>Porro officia cumque sint deleniti;</li>
                <li>Тemo facere rem vitae odit;</li>
                <li>Cum odio, iste quia doloribus autem;</li>
                <li>Aperiam nulla ea neque.</li>
              </ol>
              <input type="button" className="buy-it-now__button bg-white" value="Buy" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
