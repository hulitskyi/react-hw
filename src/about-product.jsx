import React from 'react';

export function AboutProduct() {
  return (
    <div className="container">
      <div className="row justify-content-between py-5">
        <div className="col-md-7 pr-3">
          <h2 className="font-size-30 mb-3">About your product</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga,
            illo at. Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut ipsum.
          </p>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis facilis fuga,
            illo at. Natus eos, eligendi illum rerum omnis porro ex, magni, explicabo veniam incidunt in quam sapiente ut ipsum.
          </p>
        </div>
        <div className="col-md-4">
          <div className="about-product-img" />
        </div>
      </div>
    </div>
  );
}
