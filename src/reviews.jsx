import React from 'react';


export function Reviews() {
  return (
    <div className="reviews-row py-5">
      <div className="container">
        <h2 className="text-center font-size-30 mb-4">Reviews</h2>
        <div className="row">
          <div className="col-xl-6">
            <ReviewItem />
            <ReviewItem />
          </div>
          <div className="col-xl-6">
            <ReviewItem />
            <ReviewItem />
          </div>
        </div>
      </div>
    </div>
  );
}

function ReviewItem() {
  return (
    <div className="review-item">
      <div className="reviewer-photo">
        <img alt="" />
      </div>
      <div className="review-item__text p-3">
        <div className="review-item__comment">
          <p className="font-italic">
            Porro officia cumque sint deleniti nemo facere rem vitae odit inventore cum
            odio, iste quia doloribus autem aperiam nulla ea neque reprehenderit. Libero
            doloribus, possimus officiis sapiente necessitatibus commodi consectetur?
          </p>
        </div>
        <span className="text-muted">Lourens S.</span>
      </div>
    </div>

  );
}
