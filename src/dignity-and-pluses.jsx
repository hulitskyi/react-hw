import React from 'react';

export function DignityAndPluses() {
  return (
    <div className="dignity-and-pluses">
      <div className="container">
        <div className="row py-5">
          <div className="col-md-12">
            <h2 className="text-center mb-4 font-size-30">Dignity and pluses product</h2>
            <div className="row">
              <div className="col-md-6">
                <ul className="font-size-16">
                  <li>
                    <PlusItem />
                  </li>
                  <li>
                    <PlusItem />
                  </li>
                  <li>
                    <PlusItem />
                  </li>
                </ul>
              </div>
              <div className="col-md-6">
                <ul className="font-size-16">
                  <li>
                    <PlusItem />
                  </li>
                  <li>
                    <PlusItem />
                  </li>
                  <li>
                    <PlusItem />
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function PlusItem() {
  return (
    <div className="plus-item">
      <div className="plus-icon">
        <i className="fas fa-plus-square fa-2x" />
      </div>
      <div className="plus-item_text">
        <p>
          Delectus dolorem vero quae beatae quasi dolor deserunt iste amet atque,
          impedit iure placeat, ullam.
          Reprehenderit aliquam, nemo cum velit ratione perferendis quas, maxime, quaerat porro totam,
          dolore minus inventore.
        </p>
      </div>
    </div>
  );
}
