import React from 'react';

export function Contacts() {
  return (
    <div className="contacts-row  py-5">
      <div className="container">
        <h2 className="text-center font-size-30 mb-4 text-white">Contact</h2>
        <div className="row justify-content-center">
          <div className="col-xl-5 mr-4">
            <div className="input-group mb-3">
              <input type="text" className="form-control" placeholder="Your name:" />
            </div>
            <div className="input-group mb-3">
              <input type="email" className="form-control" placeholder="Your email:" />
            </div>
            <div className="form-group">
              <textarea className="form-control" rows="6" placeholder="Your message:" />
            </div>
          </div>

          <div className="col-xl-4 text-white">
            <div className="mb-3"><i className="fab fa-skype fa-lg" />  here_your_login_skype</div>
            <div className="mb-3"><i className="fab fa-line fa-lg" />  279679659</div>
            <div className="mb-3"><i className="fas fa-at fa-lg" />  psdhtmlcss@mail.ru</div>
            <div className="mb-3"><i className="fas fa-phone fa-lg" />  80 00 4568 55 55</div>
            <div className="row">
              <div className="col-2 ">
                <button type="button" className="btn bg-white rounded-circle">
                  <i className="fab fa-twitter fa-lg" />
                </button>
              </div>
              <div className="col-2 ">
                <button type="button" className="btn bg-white rounded-circle">
                  <i className="fab fa-facebook-f fa-lg" />
                </button>
              </div>
              <div className="col-2 ">
                <button type="button" className="btn bg-white rounded-circle">
                  <i className="fab fa-linkedin-in fa-lg" />
                </button>
              </div>
              <div className="col-2 ">
                <button type="button" className="btn bg-white rounded-circle">
                  <i className="fab fa-google fa-lg" />
                </button>
              </div>
              <div className="col-2 ">
                <button type="button" className="btn bg-white rounded-circle">
                  <i className="fab fa-youtube fa-lg" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
